package utils

import "strings"

// Contains checks whether a string array contains the specified string.
func Contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

// ExtractPath returns first string after "/" and before second "/".
func ExtractPath(url string) string {
	parts := strings.SplitN(url, "/", 3)
	if len(parts) < 3 {
		return strings.Join(parts[1:], "/")
	}
	return parts[1]
}
