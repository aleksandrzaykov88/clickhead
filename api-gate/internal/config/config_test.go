package config

import (
	"fmt"
	"testing"
)

func TestConfig(t *testing.T) {
	cfg := NewConfig()
	if cfg.ApiGateAddr != "localhost:21210" {
		fmt.Println(cfg.ApiGateAddr)
		t.Error("the configuration file was not read correctly")
	}
	if cfg.AuthMsAddr != "localhost:21220" {
		fmt.Println(cfg.AuthMsAddr)
		t.Error("the .env file was not read correctly")
	}
}
