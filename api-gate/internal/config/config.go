package config

import (
	"net"
	"os"
	"path/filepath"
	"runtime"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

const (
	cfgPath = "../../config.yaml"
	envPath = "../../.env"
)

// Config represents a configuration for API-GATE.
type Config struct {
	// ApiGateAddr is the HTTP address of this application.
	ApiGateAddr string
	// AuthMsAddr is the HTTP address of AUTH-MS application.
	AuthMsAddr string
	// SomeMsAddr is the HTTP address of SOME-MS application.
	SomeMsAddr string
	// TTL is the time after which the context will be time outed.
	TTL time.Duration
}

// NewConfig returns new configuration object.
func NewConfig() *Config {
	// Set the configuration file.
	_, filename, _, _ := runtime.Caller(0)
	cfgFullPath := filepath.Join(filepath.Dir(filename), cfgPath)
	viper.SetConfigFile(cfgFullPath)

	// Read config.
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal().Err(err).Msg("reading config")
	}

	// Get application address.
	gateAddr := net.JoinHostPort(viper.GetString("api-gate.host"), viper.GetString("api-gate.port"))

	// Get timeout value for contexts.
	ttl := viper.GetDuration("api-gate.TTL")

	// Get auth-ms address from env.
	envFullPath := filepath.Join(filepath.Dir(filename), envPath)
	if err := godotenv.Load(envFullPath); err != nil {
		log.Fatal().Err(err).Msg("binding env")
	}

	// Get MS addresses from env.
	authMsAddr := os.Getenv("AUTH_MS")
	someMsAddr := os.Getenv("SOME_MS")

	return &Config{
		ApiGateAddr: gateAddr,
		AuthMsAddr:  authMsAddr,
		SomeMsAddr:  someMsAddr,
		TTL:         ttl,
	}
}
