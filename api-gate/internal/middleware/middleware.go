package middleware

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/proto"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// AccessibleURLs is a list of URLs without access verification.
var AccessibleURLs []string = []string{
	"/registration",
	"/login",
	"/refresh",
	"/check",
	"/token/check",
}

// Middleware serves the gap between http and grpc.
type Middleware struct {
	cfg *config.Config
}

// NewMiddleware is a constructor for middleware.
func NewMiddleware(cfg *config.Config) *Middleware {
	return &Middleware{
		cfg: cfg,
	}
}

// CheckAccess checks access for all private endpoints.
func (m *Middleware) CheckAccess(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if !utils.Contains(AccessibleURLs, req.RequestURI) {
			conn, err := grpc.Dial(m.cfg.AuthMsAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
			if err != nil {
				log.Error().Err(err).Msg("Auth-ms connect")
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			defer conn.Close()

			c := proto.NewAuthServiceClient(conn)

			ctx, cancel := context.WithTimeout(context.Background(), m.cfg.TTL*time.Second)
			defer cancel()

			resp, err := c.CheckAccess(ctx, &proto.CheckAccessRequest{
				RequestData: &proto.RequestData{
					AccessToken: req.Header.Get("access"),
					Url:         utils.ExtractPath(req.RequestURI),
					Method:      req.Method,
				},
			})
			if err != nil {
				log.Error().Err(err).Msg("call check access")
				w.WriteHeader(http.StatusBadRequest)
				return
			}

			if !resp.HasAccess {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}
		next.ServeHTTP(w, req)
	})
}

// RefreshToken generates request body for GRPC RefreshToken().
func (m *Middleware) RefreshToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.RequestURI == "/refresh" {
			rt := req.Header.Get("refresh")
			newBody, err := json.Marshal(map[string]string{"refresh": rt})
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			req.Body = io.NopCloser(strings.NewReader(string(newBody)))
		}
		next.ServeHTTP(w, req)
	})
}

// CheckToken generates request body for GRPC CheckToken().
func (m *Middleware) CheckToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		if req.RequestURI == "/token/check" {
			at := req.Header.Get("access")
			newBody, err := json.Marshal(map[string]string{"access": at})
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			req.Body = io.NopCloser(strings.NewReader(string(newBody)))
		}
		next.ServeHTTP(w, req)
	})
}
