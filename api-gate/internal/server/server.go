package server

import (
	"context"
	"net/http"

	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/internal/middleware"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/proto"

	"github.com/rs/zerolog/log"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// Server is a gateway server.
type Server struct {
	http.Server
}

// NewServer is a constructor for gateway server.
func NewServer(cfg *config.Config, mw *middleware.Middleware) *Server {
	// Register GRPC services endpoints.
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err := proto.RegisterItemServiceHandlerFromEndpoint(context.Background(), mux, cfg.SomeMsAddr, opts)
	if err != nil {
		log.Fatal().Err(err).Msg("Initializing Item service")
	}
	err = proto.RegisterAuthServiceHandlerFromEndpoint(context.Background(), mux, cfg.AuthMsAddr, opts)
	if err != nil {
		log.Fatal().Err(err).Msg("Initializing Auth service")
	}

	middleware := mw.CheckAccess(mux)

	return &Server{
		http.Server{
			Addr:    cfg.ApiGateAddr,
			Handler: middleware,
		},
	}
}
