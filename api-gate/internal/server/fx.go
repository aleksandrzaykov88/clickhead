package server

import (
	"context"
	"os"
	"os/signal"

	"github.com/rs/zerolog/log"
	"go.uber.org/fx"
)

var Module = fx.Module(
	"server",
	fx.Provide(
		NewServer,
	),
	fx.Invoke(
		func(lc fx.Lifecycle, srv *Server) {
			lc.Append(
				fx.Hook{
					OnStart: func(ctx context.Context) error {
						go func() {
							log.Info().Msg("API-GATEWAY service started:" + srv.Addr)
							if err := srv.Server.ListenAndServe(); err != nil {
								log.Fatal().Err(err).Msg("gateway server start")
							}
						}()
						return nil
					},
					OnStop: func(ctx context.Context) error {
						idleConnectionsClosed := make(chan struct{})
						// Starting goroutine which checks OS signals and provides graceful shutdown.
						go func() {
							sigint := make(chan os.Signal, 1)
							signal.Notify(sigint, os.Interrupt)
							<-sigint
							if err := srv.Server.Shutdown(context.Background()); err != nil {
								log.Error().Err(err).Msg("HTTP Server Shutdown Error")
							}
							close(idleConnectionsClosed)
						}()
						<-idleConnectionsClosed
						return nil
					},
				})
		},
	),
)
