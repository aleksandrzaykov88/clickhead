package main

import (
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/internal/middleware"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/internal/server"

	"go.uber.org/fx"
)

func main() {
	app := fx.New(
		fx.Options(
			server.Module,
		),
		fx.Provide(
			config.NewConfig,
			middleware.NewMiddleware,
		),
	)
	app.Run()
}
