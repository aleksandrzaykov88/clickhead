API-GATE
========

The API gateway application is accessible to the internet and processes all incoming requests. It verifies the Authorization token with the help of the auth-microservice. If authentication is successful, the user requests are then forwarded to other services.

The route for the authorization service is determined by the AUTH_MS environment variable. Alternatively, it can also be configured in the .env file.

The routes to other applications and their corresponding mappings can be found in the config.yaml configuration file or in the same .env file.
