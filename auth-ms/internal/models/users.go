package models

// RegistrationData is the type for recording a new user in the database.
type RegistrationData struct {
	Username string // User name
	PWDHash  string // sha256(password+salt)
	SaltHash string // md5(salt)
}
