package config

import (
	"net"
	"path/filepath"
	"runtime"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

const cfgPath = "../../config.yaml"

// Config is the AUTH-MS configuration file.
type Config struct {
	// AuthMsAddr is the HTTP address for AUTH-ms.
	AuthMsAddr string
	// TTL is timeout for context.
	TTL time.Duration
	// AccessTTL is timeout for access token.
	AccessTTL time.Duration
	// TTL is timeout for refresh token.
	RefreshTTL time.Duration
	// DB is the database configuration.
	DB *ConfigDB `yaml:"db"`
}

// ConfigDB represents database config.
type ConfigDB struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	DBName   string `yaml:"dbname"`
}

// NewConfig is the constructor for AUTH-MS configuration.
func NewConfig() *Config {
	// Set the configuration file.
	_, filename, _, _ := runtime.Caller(0)
	cfgFullPath := filepath.Join(filepath.Dir(filename), cfgPath)
	viper.SetConfigFile(cfgFullPath)

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Msg("read config: " + err.Error())
	}

	// Get AUTH-MS address.
	authMsAddr := net.JoinHostPort(viper.GetString("auth-ms.host"), viper.GetString("auth-ms.port"))

	// Get timeout values.
	ttl := viper.GetDuration("auth-ms.TTL")
	attl := viper.GetDuration("auth-ms.accessTTL")
	rttl := viper.GetDuration("auth-ms.refreshTTL")

	// Get DB configuration data.
	cfg := &Config{}
	err = viper.Unmarshal(cfg)
	if err != nil {
		log.Fatal().Msg("database config: " + err.Error())
	}
	cfg.AuthMsAddr = authMsAddr
	cfg.TTL = ttl
	cfg.AccessTTL = attl
	cfg.RefreshTTL = rttl

	return cfg
}
