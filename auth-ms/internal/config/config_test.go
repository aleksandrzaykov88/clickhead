package config

import (
	"fmt"
	"testing"
)

func TestConfig(t *testing.T) {
	cfg := NewConfig()
	if cfg.AuthMsAddr != "localhost:21220" {
		fmt.Println(cfg.AuthMsAddr)
		t.Error("the configuration file was not read correctly")
	}
	if cfg.DB.DBName != "authmsdb" {
		fmt.Println(cfg.DB.DBName)
		t.Error("the configuration file was not read correctly")
	}
}
