package tokenman

import (
	"errors"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/golang-jwt/jwt/v5"
	"github.com/joho/godotenv"
)

// TokenManager describes all operations with JWT tokens.
type TokenManager interface {
	NewJWT(userID, roleID int, ttl time.Duration) (string, error)
	Parse(accessToken string) (string, error)
	NewRefreshToken() (string, error)
}

// Manager implements TokenManager.
type Manager struct {
	signKey string
}

// NewManager returns new Manager object.
func NewManager() *Manager {
	_, filename, _, _ := runtime.Caller(0)
	envFullPath := filepath.Join(filepath.Dir(filename), "../../.env")
	if err := godotenv.Load(envFullPath); err != nil {
		log.Fatal().Err(err).Msg("binding env")
	}
	key := os.Getenv("SECRET_KEY")
	return &Manager{
		signKey: key,
	}
}

// NewJWT creates and returnes new JWT token.
func (m *Manager) NewJWT(userID int, ttl time.Duration) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(ttl)),
		Subject:   strconv.Itoa(userID),
	})

	return token.SignedString([]byte(m.signKey))
}

// Parse validate access token and returns payload.
func (m *Manager) Parse(accessToken string) (string, error) {
	token, err := jwt.Parse(accessToken, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %s", t.Header)
		}
		return []byte(m.signKey), nil
	})
	if err != nil {
		return "", err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("get claims")
	}

	return claims["sub"].(string), nil
}

// NewRefreshToken creates and return new refresh token.
func (m *Manager) NewRefreshToken() (string, error) {
	b := make([]byte, 32)

	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s)

	_, err := r.Read(b)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", b), nil
}
