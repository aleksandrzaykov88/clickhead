package users

import (
	"context"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"time"

	_ "github.com/lib/pq"
	"github.com/nullism/bqb"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/database"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/models"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/tokenman"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/pkg/utils"
)

// UsersRepo is a repository for working with User entity.
type UsersRepo struct {
	db  *sql.DB
	tm  *tokenman.Manager
	cfg *config.Config
}

// NewUsersRepo returns database object for use it in handlers.
func NewUsersRepo(db *database.DB, tm *tokenman.Manager, cfg *config.Config) *UsersRepo {
	return &UsersRepo{
		db:  db.DB,
		tm:  tm,
		cfg: cfg,
	}
}

// RegisterUser register new user in DB and returns it.
func (ur *UsersRepo) RegisterUser(ctx context.Context, user *models.RegistrationData) (int, error) {
	if user == nil || user.Username == "" || user.PWDHash == "" || user.SaltHash == "" {
		return 0, errors.New("empty user data")
	}

	q := bqb.New("select * from fn_user_ins(?,?,?)", user.Username, user.PWDHash, user.SaltHash)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return 0, err
	}

	var userID int
	if err := ur.db.QueryRowContext(ctx, queryString, params...).Scan(&userID); err != nil {
		return 0, err
	}

	return userID, err
}

// Login user and create session in DB.
func (ur *UsersRepo) Login(ctx context.Context, userName, password string) (string, string, error) {
	if userName == "" || password == "" {
		return "", "", errors.New("empty user data")
	}

	var (
		id            int
		salt, pwdHash string
	)

	q := bqb.New("select u.id, u.password_salt, u.password_hash from t_user u where u.login = (?)", userName)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return "", "", err
	}
	if err := ur.db.QueryRowContext(ctx, queryString, params...).Scan(&id, &salt, &pwdHash); err != nil {
		return "", "", err
	}

	// Decrypting salt.
	decodedSalt, err := base64.StdEncoding.DecodeString(salt)
	if err != nil {
		return "", "", err
	}

	// Checking password.
	hashedPassword, err := utils.SaltedPasswordHash(password, string(decodedSalt))
	if err != nil {
		return "", "", err
	}
	if hashedPassword != pwdHash {
		return "", "", errors.New("wrong password")
	}

	// Generate pair of Access and Refresh tokens
	at, err := ur.tm.NewJWT(id, ur.cfg.AccessTTL*time.Minute)
	if err != nil {
		return "", "", fmt.Errorf("jwt: %w", err)
	}

	rt, err := ur.tm.NewRefreshToken()
	if err != nil {
		return "", "", fmt.Errorf("refresh: %w", err)
	}

	// Writing new session in database
	q = bqb.New("select * from fn_create_session(?,?,?)", id, rt, time.Now().Add(ur.cfg.RefreshTTL*time.Minute))
	queryString, params, err = q.ToPgsql()
	if err != nil {
		return "", "", err
	}
	if _, err := ur.db.ExecContext(ctx, queryString, params...); err != nil {
		return "", "", err
	}

	return at, rt, nil
}

// RefreshToken refreshes token in DB and returns pair of Refresh and Access tokens.
func (ur *UsersRepo) RefreshToken(ctx context.Context, refreshToken string) (string, string, error) {
	if refreshToken == "" {
		return "", "", errors.New("empty user data")
	}

	// Generate new Refresh tokens
	rt, err := ur.tm.NewRefreshToken()
	if err != nil {
		return "", "", fmt.Errorf("refresh: %w", err)
	}

	q := bqb.New("select * from fn_refresh_session(?,?,?)", refreshToken, rt, time.Now().Add(ur.cfg.RefreshTTL*time.Minute))
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return "", "", err
	}

	var userID int
	if err := ur.db.QueryRowContext(ctx, queryString, params...).Scan(&userID); err != nil {
		return "", "", err
	}

	// Generating new Access Token
	at, err := ur.tm.NewJWT(userID, ur.cfg.AccessTTL*time.Minute)
	if err != nil {
		return "", "", fmt.Errorf("jwt: %w", err)
	}

	return at, rt, nil
}

// CheckAccess checks availabity for user role to visit some endpoint.
func (ur *UsersRepo) CheckAccess(ctx context.Context, accessToken, url, httpMethod string) (bool, error) {
	if accessToken == "" || url == "" || httpMethod == "" {
		return false, errors.New("empty user data")
	}

	// JWT token check and getting user id.
	sub, err := ur.tm.Parse(accessToken)
	if err != nil {
		return false, err
	}
	userID, err := strconv.Atoi(sub)
	if err != nil {
		return false, err
	}

	q := bqb.New("select * from fn_check_access(?,?,?)", userID, httpMethod, url)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return false, err
	}

	var isAccessed bool
	if err := ur.db.QueryRowContext(ctx, queryString, params...).Scan(&isAccessed); err != nil {
		return false, err
	}

	return isAccessed, nil
}

// CheckToken validates access token.
func (ur *UsersRepo) CheckToken(ctx context.Context, accessToken string) (bool, error) {
	if accessToken == "" {
		return false, errors.New("empty access token")
	}

	// JWT token check.
	if _, err := ur.tm.Parse(accessToken); err != nil {
		return false, err
	}

	return true, nil
}
