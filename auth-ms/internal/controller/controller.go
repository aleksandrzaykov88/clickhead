package controller

import (
	"context"
	"encoding/base64"
	"errors"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/xyproto/randomstring"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/proto"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/models"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/users"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/pkg/utils"
)

// Controller is GRPC server with handlers.
type Controller struct {
	proto.UnimplementedAuthServiceServer
	users *users.UsersRepo
	TTL   time.Duration
}

// NewController is constructor for Controller.
func NewController(cfg *config.Config, users *users.UsersRepo) *Controller {
	return &Controller{
		TTL:   cfg.TTL,
		users: users,
	}
}

// RegisterUser is the handler which register a new user.
func (c *Controller) RegisterUser(ctx context.Context, request *proto.RegisterUserRequest) (*proto.RegisterUserResponse, error) {
	if request.UserData == nil {
		log.Error().Msg("empty request")
		return nil, errors.New("empty request")
	}

	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	// Preparing salt for db write.
	salt := randomstring.HumanFriendlyEnglishString(5)
	encodedSalt := base64.StdEncoding.EncodeToString([]byte(salt))
	// Preparing password for db write.
	hashedPassword, err := utils.SaltedPasswordHash(request.UserData.Password, salt)
	if err != nil {
		log.Error().Err(err).Msg("register user")
		return nil, err
	}
	// Preparing registration data.
	userData := &models.RegistrationData{
		Username: request.UserData.Username,
		PWDHash:  hashedPassword,
		SaltHash: encodedSalt,
	}

	userID, err := c.users.RegisterUser(ctx, userData)
	if err != nil {
		log.Error().Err(err).Msg("register user")
		return nil, err
	}

	return &proto.RegisterUserResponse{
		Id: int64(userID),
	}, nil
}

// Login is the handler which logging user.
func (c *Controller) Login(ctx context.Context, request *proto.LoginRequest) (*proto.LoginResponse, error) {
	if request.UserData == nil {
		log.Error().Msg("empty request")
		return nil, errors.New("empty request")
	}

	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	accessToken, refreshToken, err := c.users.Login(ctx, request.UserData.Username, request.UserData.Password)
	if err != nil {
		log.Error().Err(err).Msg("login")
		return nil, err
	}

	return &proto.LoginResponse{
		Tokens: &proto.TokenPair{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		},
	}, nil
}

// RefreshToken is the handler which refresh token and returns pair of tokens.
func (c *Controller) RefreshToken(ctx context.Context, request *proto.TokenRequest) (*proto.TokenResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	accessToken, refreshToken, err := c.users.RefreshToken(ctx, request.RefreshToken)
	if err != nil {
		log.Error().Err(err).Msg("refreshing token")
		return nil, err
	}

	return &proto.TokenResponse{
		Tokens: &proto.TokenPair{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		},
	}, nil
}

// CheckAccess is a handler that checks whether endpoint is available for user.
func (c *Controller) CheckAccess(ctx context.Context, request *proto.CheckAccessRequest) (*proto.CheckAccessResponse, error) {
	if request.RequestData == nil {
		log.Error().Msg("empty request")
		return nil, errors.New("empty request")
	}

	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	hasAccess, err := c.users.CheckAccess(ctx, request.RequestData.AccessToken, request.RequestData.Url, request.RequestData.Method)
	if err != nil {
		log.Error().Err(err).Msg("checking access")
		return nil, err
	}

	return &proto.CheckAccessResponse{
		HasAccess: hasAccess,
	}, nil
}

// CheckToken is a handler that validates access token.
func (c *Controller) CheckToken(ctx context.Context, request *proto.CheckTokenRequest) (*proto.CheckTokenResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	hasAccess, err := c.users.CheckToken(ctx, request.AccessToken)
	if err != nil {
		log.Error().Err(err).Msg("checking token")
		return nil, err
	}

	return &proto.CheckTokenResponse{
		HasAccess: hasAccess,
	}, nil
}
