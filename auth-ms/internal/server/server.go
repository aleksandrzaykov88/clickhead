package server

import (
	"net"

	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/proto"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/controller"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/database"

	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Server is a GRPC server.
type Server struct {
	listener *net.Listener
	*grpc.Server
}

// NewServer is a constructor for GRPC server.
func NewServer(cfg *config.Config, db *database.DB, controller *controller.Controller) *Server {
	listener, err := net.Listen("tcp", cfg.AuthMsAddr)
	if err != nil {
		log.Fatal().Err(err)
	}
	s := grpc.NewServer()
	reflection.Register(s)
	proto.RegisterAuthServiceServer(s, controller)

	return &Server{
		listener: &listener,
		Server:   s,
	}
}
