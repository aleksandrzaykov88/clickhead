package main

import (
	_ "github.com/swaggo/echo-swagger/example/docs"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/controller"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/database"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/server"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/tokenman"
	"gitlab.com/aleksandrzaykov88/clickhead/auth-ms/internal/users"
	"go.uber.org/fx"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

func main() {
	app := fx.New(
		fx.Options(
			server.Module,
		),
		fx.Provide(
			config.NewConfig,
			controller.NewController,
			users.NewUsersRepo,
			database.NewDB,
			tokenman.NewManager,
		),
	)
	app.Run()
}
