package utils

import (
	"crypto/sha256"
	"encoding/hex"
)

// SaltedPasswordHash returns sha256(password+salt).
func SaltedPasswordHash(password, salt string) (string, error) {
	hasher := sha256.New()
	_, err := hasher.Write([]byte(password + salt))
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(hasher.Sum(nil)), nil
}
