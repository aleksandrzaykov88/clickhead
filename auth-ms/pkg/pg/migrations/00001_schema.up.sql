CREATE OR REPLACE FUNCTION z_slug(arg_text text)
    RETURNS text as
$BODY$
DECLARE
    output_text text;
BEGIN
    output_text := translate(arg_text,
                             'абвгдежзийклмнопрстуфхцчшщъыьэюя',
                             'abvgdegziyklmnoprstufhzcss_y_eua');
    output_text := regexp_replace(output_text, '[^a-zA-Z0-9]+', '-', 'g');
    output_text := regexp_replace(output_text, '^-|-$', '', 'g');
    output_text := lower(output_text);

    RETURN output_text;
END;
$BODY$
    LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION z_exception_if(
    condition boolean,
    message text
)
    RETURNS VOID AS $$
BEGIN
    IF condition THEN
        RAISE EXCEPTION '%', message;
    END IF;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fn_user_ins(
    arg_login text,
    arg_password_hash text,
    arg_salt_hash text
)
    RETURNS INT AS
$$
declare
    v_id int;
    v_role_id int;
begin
    perform z_exception_if(arg_login is null or arg_login = '', 'Login is empty');
    perform z_exception_if(arg_password_hash is null or arg_password_hash = '', 'Password is empty');
    perform z_exception_if(arg_salt_hash is null or arg_salt_hash = '', 'Salt is empty');
    perform z_exception_if(
        exists (
                select 1
                from t_user u
                where u.login = arg_login
            ),
        'Login already exists'
    );

    select r.id
    from t_role r
    where r.name = 'user'
    into v_role_id;

    insert into t_user as u (
        login,
        password_hash,
        password_salt,
        role_id
    ) values (
        arg_login,
        arg_password_hash,
        arg_salt_hash,
        v_role_id
    ) returning u.id into v_id;

    return v_id;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_user_ins(text, text, text) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_user_ins(text, text, text) IS 'Users. Registration';
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE t_user (
                        id SERIAL PRIMARY KEY,
                        login TEXT NOT NULL,
                        password_hash TEXT NOT NULL,
                        password_salt TEXT NOT NULL,
                        role_id INT
);
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE t_role (
    id INT PRIMARY KEY,
    name TEXT NOT NULL
);
------------------------------------------------------------------------------------------------------------------------
INSERT INTO t_role(id, name)
VALUES (1, 'admin'), (2, 'user'), (3, 'guest');
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW public.v_user AS
SELECT
    u.id,
    u.login,
    r.name as role
FROM t_user u
JOIN t_role r ON r.id = u.role_id
ORDER BY u.id;
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE t_role_permissions (
    role_id INT,
    endpoint_id INT,
    read boolean,
    write boolean,
    delete boolean
);
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE t_address(
    id int PRIMARY KEY,
    endpoint VARCHAR(255)
);
------------------------------------------------------------------------------------------------------------------------
INSERT INTO t_address(id, endpoint)
VALUES (1, 'item'), (2, 'items');
------------------------------------------------------------------------------------------------------------------------
INSERT INTO t_role_permissions(role_id, endpoint_id, read, write, delete)
VALUES
    (1, 1, true, true, true),
    (1, 2, true, true, true),
    (2, 1, true, true, false),
    (2, 2, true, true, false),
    (3, 1, true, false, false),
    (3, 2, false, false, false);
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE t_session(
    id SERIAL,
    user_id int,
    refresh_token text,
    expires timestamp
);
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fn_create_session(
    arg_user_id int,
    arg_refresh_token text,
    arg_expires timestamp
)
    RETURNS VOID AS
$$
begin
    perform z_exception_if(arg_user_id is null, 'User ID is empty');
    perform z_exception_if(arg_refresh_token is null or arg_refresh_token = '', 'Refresh token is empty');
    perform z_exception_if(
        not exists (
                select 1
                from t_user u
                where u.id = arg_user_id
            ),
        'There is no such user in db'
    );

    delete from t_session s
    where s.user_id = arg_user_id;

    insert into t_session as s (
        user_id,
        refresh_token,
        expires
    ) values (
        arg_user_id,
        arg_refresh_token,
        coalesce(arg_expires, CURRENT_DATE)
    );
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_create_session(int, text, timestamp) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_create_session(int, text, timestamp) IS 'Sessions. Create';
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fn_refresh_session(
    arg_refresh_token_old text,
    arg_refresh_token_new text,
    arg_expires timestamp
)
    RETURNS INT AS
$$
declare
    v_id int;
begin
    perform z_exception_if(arg_refresh_token_old is null or arg_refresh_token_old = '', 'Refresh token is empty');
    perform z_exception_if(arg_refresh_token_new is null or arg_refresh_token_new = '', 'Refresh token is empty');
    perform z_exception_if(
        not exists (
                select 1
                from t_session s
                where s.refresh_token = arg_refresh_token_old
            ),
        'There is no such session in db'
    );

    update t_session set
        refresh_token = arg_refresh_token_new,
        expires = coalesce(arg_expires, CURRENT_DATE)
    returning user_id into v_id;

    return v_id;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_refresh_session(text, text, timestamp) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_refresh_session(text, text, timestamp) IS 'Sessions. Refresh';
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fn_check_access(
    arg_user_id int,
    arg_http_method text,
    arg_url text
)
    RETURNS BOOLEAN AS
$$
declare
    v_read boolean;
    v_write boolean;
    v_delete boolean;
    v_access boolean;
begin
    perform z_exception_if(arg_user_id is null, 'User id is empty');
    perform z_exception_if(arg_http_method is null or arg_http_method = '', 'HTTP Method is empty');
    perform z_exception_if(arg_url is null or arg_url = '', 'URL is empty');
    perform z_exception_if(
        not exists (
                select 1
                from t_user u
                where u.id = arg_user_id
            ),
        'There is no such user in db'
    );
    perform z_exception_if(
        not exists (
                select 1
                from t_address a
                where a.endpoint = arg_url
            ),
        'There is no such address in db'
    );

    select rp.read, rp.write, rp.delete from t_role_permissions rp
    join t_user u on u.role_id = rp.role_id
    join t_address a on a.id = rp.endpoint_id
    where a.endpoint = arg_url and u.id = arg_user_id
    into v_read, v_write, v_delete;

    v_access = false;
    CASE arg_http_method
        WHEN 'GET' THEN 
           	if v_read then 
           		v_access = true;
           	end if;
        WHEN 'POST' THEN 
            if v_write then 
           		v_access = true;
           	end if;
        WHEN 'PATCH' THEN 
            if v_write then 
           		v_access = true;
           	end if;
        WHEN 'DELETE' THEN 
            if v_delete then 
           		v_access = true;
           	end if;
        ELSE
            RAISE EXCEPTION '%', 'unsupporter http method';
    END CASE;

    return v_access;
end;
$$
    LANGUAGE plpgsql;

ALTER FUNCTION public.fn_refresh_session(text, text, timestamp) OWNER TO postgres;
COMMENT ON FUNCTION public.fn_refresh_session(text, text, timestamp) IS 'Access. Check';