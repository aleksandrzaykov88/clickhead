Clickhead Test Task
===================

First Launch
------------

A batch of microservices is launched using _docker-compose_ (file in the root of the project).
Before starting to interact, it is necessary to perform migrations by running the corresponding _Makefiles_ in **auth-ms** 
and **some-db**. After that, you can start testing the requests.

Features
--------

An important note is that, at the moment, the **api-gate** service handles token mechanics using HTTP headers.

For _Access Token_, this is the `access` header;
For _Refresh Token_, this is the `refresh` header.