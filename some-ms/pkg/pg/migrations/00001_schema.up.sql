CREATE OR REPLACE FUNCTION z_slug(arg_text text)
    RETURNS text as
$BODY$
DECLARE
    output_text text;
BEGIN
    output_text := translate(arg_text,
                             'абвгдежзийклмнопрстуфхцчшщъыьэюя',
                             'abvgdegziyklmnoprstufhzcss_y_eua');
    output_text := regexp_replace(output_text, '[^a-zA-Z0-9]+', '-', 'g');
    output_text := regexp_replace(output_text, '^-|-$', '', 'g');
    output_text := lower(output_text);

    RETURN output_text;
END;
$BODY$
    LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION z_exception_if(
    condition boolean,
    message text
)
    RETURNS VOID AS $$
BEGIN
    IF condition THEN
        RAISE EXCEPTION '%', message;
    END IF;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fn_item_create(
    arg_item_info TEXT,
    arg_item_price INT
) RETURNS TABLE (item_id INT, item_info TEXT, item_price INT)
AS $$
DECLARE
    v_id INT;
BEGIN
    INSERT INTO t_item (info, price)
    VALUES (arg_item_info, arg_item_price)
    RETURNING id INTO v_id;

    RETURN QUERY
        SELECT
            id AS item_id,
            info AS item_info,
            price AS item_price
        FROM t_item
        WHERE id = v_id;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS t_item (
                                      id SERIAL PRIMARY KEY,
                                      info TEXT NOT NULL,
                                      price INT NOT NULL
);

CREATE INDEX item_id_idx ON t_item (id);

ALTER TABLE t_item ALTER COLUMN price SET DEFAULT 0;

ALTER TABLE t_item ADD CONSTRAINT price_positive CHECK (price >= 0);
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fn_item_get(arg_id int)
    RETURNS TABLE (item_id INT, item_info TEXT, item_price INT)
AS $body$
BEGIN
    RETURN QUERY
        SELECT
            id AS item_id,
            info AS item_info,
            price AS item_price
        FROM t_item
        WHERE id = arg_id;
END;
$body$
    LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fn_items_get()
    RETURNS TABLE (item_id INT, item_info TEXT, item_price INT)
AS $body$
BEGIN
    RETURN QUERY
        SELECT
            id AS item_id,
            info AS item_info,
            price AS item_price
        FROM t_item;
END;
$body$
    LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fn_item_update(
    arg_id INTEGER,
    arg_item_info TEXT,
    arg_item_price INT
) RETURNS TABLE (item_id INT, item_info TEXT, item_price INT)
AS $$
BEGIN
    UPDATE t_item SET
                      info = coalesce(arg_item_info, info),
                      price = coalesce(arg_item_price, price)
    WHERE id = arg_id;

    RETURN QUERY
        SELECT
            id AS item_id,
            info AS item_info,
            price AS item_price
        FROM t_item
        WHERE id = arg_id;
END;
$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION fn_item_delete(
    arg_id INTEGER
) RETURNS VOID
AS $$
BEGIN
    DELETE FROM t_item
    WHERE id = arg_id;
END;
$$ LANGUAGE plpgsql;