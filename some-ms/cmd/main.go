package main

import (
	_ "github.com/swaggo/echo-swagger/example/docs"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/controller"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/database"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/items"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/server"
	"go.uber.org/fx"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

func main() {
	app := fx.New(
		fx.Options(
			server.Module,
		),
		fx.Provide(
			config.NewConfig,
			controller.NewController,
			items.NewItemsRepo,
			database.NewDB,
		),
	)
	app.Run()
}
