package items

import (
	"context"
	"database/sql"

	_ "github.com/lib/pq"
	"github.com/nullism/bqb"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/proto"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/database"
)

// ItemsRepo is a repository for working with Item entity.
type ItemsRepo struct {
	db *sql.DB
}

// NewItemsRepo returns database object for use it in handlers.
func NewItemsRepo(db *database.DB) *ItemsRepo {
	return &ItemsRepo{
		db: db.DB,
	}
}

// GetItems getting items from db.
func (ir *ItemsRepo) GetItems(ctx context.Context) (items []*proto.Item, err error) {
	q := bqb.New("select * from fn_items_get()")
	queryString, _, err := q.ToPgsql()
	if err != nil {
		return nil, err
	}

	rows, err := ir.db.QueryContext(ctx, queryString)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var Items = make([]*proto.Item, 0)
	for rows.Next() {
		var item proto.Item
		err := rows.Scan(&item.Id, &item.Info, &item.Price)
		if err != nil {
			return nil, err
		}
		Items = append(Items, &item)
	}

	return Items, nil
}

// GetItem get single item from db.
func (ir *ItemsRepo) GetItem(ctx context.Context, id int32) (item *proto.Item, err error) {
	q := bqb.New("select * from fn_item_get(?)", id)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return nil, err
	}

	var Item proto.Item
	if err := ir.db.QueryRowContext(ctx, queryString, params...).Scan(&Item.Id, &Item.Info, &Item.Price); err != nil {
		return nil, err
	}

	return &Item, err
}

// CreateItem creates new item in db.
func (ir *ItemsRepo) CreateItem(ctx context.Context, item *proto.Item) (i *proto.Item, err error) {
	q := bqb.New("select * from fn_item_create(?,?)", item.Info, item.Price)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return nil, err
	}

	var Item proto.Item
	if err := ir.db.QueryRowContext(ctx, queryString, params...).Scan(&Item.Id, &Item.Info, &Item.Price); err != nil {
		return nil, err
	}

	return &Item, err
}

// UpdateItem updates item info in db.
func (ir *ItemsRepo) UpdateItem(ctx context.Context, item *proto.Item) (i *proto.Item, err error) {
	q := bqb.New("select * from fn_item_update(?,?,?)", item.Id, item.Info, item.Price)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return nil, err
	}

	var Item proto.Item
	if err := ir.db.QueryRowContext(ctx, queryString, params...).Scan(&Item.Id, &Item.Info, &Item.Price); err != nil {
		return nil, err
	}

	return &Item, err
}

// DeleteItem removes item from db.
func (ir *ItemsRepo) DeleteItem(ctx context.Context, id int32) (err error) {
	q := bqb.New("select * from fn_item_delete(?)", id)
	queryString, params, err := q.ToPgsql()
	if err != nil {
		return err
	}
	_, err = ir.db.ExecContext(ctx, queryString, params...)
	if err != nil {
		return err
	}

	return nil
}
