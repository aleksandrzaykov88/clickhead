package database

import (
	"database/sql"
	"fmt"

	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/config"

	_ "github.com/lib/pq"
)

// DB is a wrapper for sql databas.
type DB struct {
	*sql.DB
}

// NewDB create and return database connection entity.
func NewDB(cfg *config.Config) (*DB, error) {
	psqlInfo := fmt.Sprintf("user=%s password=%s dbname=%s host=db-some sslmode=disable",
		cfg.DB.User,
		cfg.DB.Password,
		//cfg.DB.Port,
		//cfg.DB.Host,
		cfg.DB.DBName,
	)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}
	return &DB{
		DB: db,
	}, nil
}
