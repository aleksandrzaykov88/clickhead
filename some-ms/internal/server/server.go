package server

import (
	"net"

	"github.com/rs/zerolog/log"

	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/proto"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/controller"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Server is a GRPC server.
type Server struct {
	listener *net.Listener
	*grpc.Server
}

// NewServer returns a Server entity.
func NewServer(cfg *config.Config, controller *controller.Controller) *Server {
	listener, err := net.Listen("tcp", cfg.SomeMsAddr)
	if err != nil {
		log.Fatal().Err(err)
	}
	s := grpc.NewServer()
	reflection.Register(s)
	proto.RegisterItemServiceServer(s, controller)

	return &Server{
		listener: &listener,
		Server:   s,
	}
}
