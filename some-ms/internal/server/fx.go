package server

import (
	"context"
	"time"

	"github.com/rs/zerolog/log"
	"go.uber.org/fx"
)

var Module = fx.Module(
	"server",
	fx.Provide(
		NewServer,
	),
	fx.Invoke(
		func(lc fx.Lifecycle, srv *Server) {
			lc.Append(
				fx.Hook{
					OnStart: func(ctx context.Context) error {
						go func() {
							log.Info().Msg("SOME-MS service started")
							if err := srv.Serve(*srv.listener); err != nil {
								log.Fatal().Err(err).Msg("item server start")
							}
						}()
						return nil
					},
					OnStop: func(ctx context.Context) error {
						// Graceful shutdown.
						ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
						defer cancel()
						srv.GracefulStop()
						<-ctx.Done()
						return nil
					},
				})
		},
	),
)
