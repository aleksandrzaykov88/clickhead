package config

import (
	"fmt"
	"testing"
)

func TestConfig(t *testing.T) {
	cfg := NewConfig()
	if cfg.SomeMsAddr != "localhost:21230" {
		fmt.Println(cfg.SomeMsAddr)
		t.Error("the configuration file was not read correctly")
	}
	if cfg.DB.DBName != "somemsdb" {
		fmt.Println(cfg.DB.DBName)
		t.Error("the configuration file was not read correctly")
	}
}
