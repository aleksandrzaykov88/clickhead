package config

import (
	"net"
	"path/filepath"
	"runtime"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

const cfgPath = "../../config.yaml"

// Config is the AUTH-MS configuration file.
type Config struct {
	// SomeMsAddr is the HTTP address for SOME-ms app.
	SomeMsAddr string
	// TTL is timeout for context.
	TTL time.Duration
	// DB is the database configuration.
	DB *ConfigDB `yaml:"db"`
}

// ConfigDB represents database config.
type ConfigDB struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	DBName   string `yaml:"dbname"`
}

// NewConfig is the constructor for SOME-MS configuration.
func NewConfig() *Config {
	// Set the configuration file.
	_, filename, _, _ := runtime.Caller(0)
	cfgFullPath := filepath.Join(filepath.Dir(filename), cfgPath)
	viper.SetConfigFile(cfgFullPath)

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Msg("read config: " + err.Error())
	}

	// Get SOME-MS address.
	someMsAddr := net.JoinHostPort(viper.GetString("some-ms.host"), viper.GetString("some-ms.port"))

	// Get timeout value for contexts.
	ttl := viper.GetDuration("some-ms.TTL")

	// Get DB configuration data.
	cfg := &Config{}
	err = viper.Unmarshal(cfg)
	if err != nil {
		log.Fatal().Msg("database config: " + err.Error())
	}
	cfg.SomeMsAddr = someMsAddr
	cfg.TTL = ttl

	return cfg
}
