package controller

import (
	"context"
	"errors"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/aleksandrzaykov88/clickhead/api-gate/proto"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/config"
	"gitlab.com/aleksandrzaykov88/clickhead/some-ms/internal/items"
)

// Controller it is GRPC server with handlers.
type Controller struct {
	proto.UnimplementedItemServiceServer
	TTL   time.Duration
	items *items.ItemsRepo
}

// NewController it is the constructor for Controller.
func NewController(cfg *config.Config, items *items.ItemsRepo) *Controller {
	return &Controller{
		TTL:   cfg.TTL,
		items: items,
	}
}

// GetItems fetch a bunch of Items.
func (c *Controller) GetItems(ctx context.Context, req *proto.GetItemsRequest) (*proto.GetItemsResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	items, err := c.items.GetItems(ctx)
	if err != nil {
		log.Error().Err(err).Msg("getting items")
		return nil, err
	}

	return &proto.GetItemsResponse{
		Items: items,
	}, nil
}

// GetItem fetch a single item by id.
func (c *Controller) GetItem(ctx context.Context, req *proto.GetItemRequest) (*proto.GetItemResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, config.NewConfig().TTL*time.Second)
	defer cancel()

	item, err := c.items.GetItem(ctx, req.Id)
	if err != nil {
		log.Error().Err(err).Msg("getting item")
		return nil, err
	}

	return &proto.GetItemResponse{
		Item: item,
	}, nil
}

// CreateItem is a handler which creates item in DB.
func (c *Controller) CreateItem(ctx context.Context, req *proto.CreateItemRequest) (*proto.CreateItemResponse, error) {
	if req.Item == nil {
		log.Error().Msg("empty request")
		return nil, errors.New("empty request")
	}

	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	item, err := c.items.CreateItem(ctx, req.Item)
	if err != nil {
		log.Error().Err(err).Msg("creating item")
		return nil, err
	}

	return &proto.CreateItemResponse{
		Item: item,
	}, nil
}

// UpdateItem is a handler which update item in DB.
func (c *Controller) UpdateItem(ctx context.Context, req *proto.UpdateItemRequest) (*proto.UpdateItemResponse, error) {
	if req.Item == nil {
		log.Error().Msg("empty request")
		return nil, errors.New("empty request")
	}

	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	item, err := c.items.UpdateItem(ctx, req.Item)
	if err != nil {
		log.Error().Err(err).Msg("updating item")
		return nil, err
	}

	return &proto.UpdateItemResponse{
		Item: item,
	}, nil
}

// DeleteItem is a handler which deletes item from DB.
func (c *Controller) DeleteItem(ctx context.Context, req *proto.DeleteItemRequest) (*proto.DeleteItemResponse, error) {
	ctx, cancel := context.WithTimeout(ctx, c.TTL*time.Second)
	defer cancel()

	if err := c.items.DeleteItem(ctx, req.Id); err != nil {
		log.Error().Err(err).Msg("deleting item")
		return nil, err
	}

	return &proto.DeleteItemResponse{
		Message: "item deleted",
	}, nil
}
